# CI/CD component for the IGWN Software Archive

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to upload files to the
[IGWN Software Archive](https://computing.docs.ligo.org/guide/software/distribution).

[[_TOC_]]

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml`
file by using the `include:` keyword, where `<VERSION>` is the latest released
tag.

See the
[`example/`](https://git.ligo.org/computing/gitlab/components/igwn-archive/-/tree/main/example/)
project for a demonstration of how this component set may be used in a real
project.

### Components

#### upload

Configure a job to upload one or more files to the IGWN Software Archive:

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/igwn-archive/upload@<VERSION>
    inputs:
      stage: deploy
      needs: [sdist]
      files: "myproject-*.tar.*"
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `deploy` | The pipeline stage to add jobs to |
| `image` | [`igwn/archive`](https://hub.docker.com/r/igwn/archive) | The container image to use |
| `needs` |  | [REQUIRED] List of jobs whose artifacts are to be uploaded |
| `files` |  | [REQUIRED] Filename or glob pattern to upload |
| `igwn_archive_user` | `$GITLAB_USER_LOGIN` | Username to authorise upload; this should be the username associated with the SSH private key |
| `igwn_archive_host` | `software.igwn.org` | The host to upload to |
| `igwn_archive_options` | `"--verbose --verbose"` | Options to pass to `igwn-source-upload` |

**Additionally** this job should be configured using the following
[protected, masked variables](https://git.ligo.org/help/ci/variables/index.html#cicd-variable-security):

| Name                           | Purpose |
| ------------------------------ | ------- |
| `IGWN_ARCHIVE_RSYNC_PASSWORD`  | The password to use when communicating over `rsync`, see <https://secrets.ligo.org/secrets/433/> |
| `IGWN_ARCHIVE_SSH_PRIVATE_KEY` | The SSH private key (file or `base64` encoded string) to use when connecting to the archive, see [here](https://git.ligo.org/help/ci/ssh_keys/index.html#ssh-keys-when-using-the-docker-executor) |

Notes:

-   Protected variables only work on protected branches or tags, so you must
    ensure that if you configure the CI/CD variables as 'Protected', you also
    configure the branch or tag you will push to to be protected.
    For more details, see
    <https://git.ligo.org/help/ci/variables/index.html#protect-a-cicd-variable>.

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
